# Change.org Accurate Signature

Show an accurate number of signatures in change.org.

### Chrome

1. `npm install`
2. `npm run build chrome`
3. Open "chrome://extensions" in Chrome.
4. Enable Developer Mode.
5. Click "LOAD UNPACKED" and select `dist/chrome`.

### Firefox

1. `npm install`
2. `npm run build firefox`
3. Open "about:debugging" in Firefox.
4. Click "LOAD UNPACKED" and select `dist/firefox/manifest.json`.
