const div = document.querySelector("#content div[data-view=\"petitions/show/components/gradient_thermometer\"]")
const displayd_count = parseInt(div.getAttribute("data-displayed_count"), 10)
const following_text = div.querySelector("strong").innerText.split(' ').slice(1).join(' ')

const accurate = document.createElement('h1')
accurate.innerText = `${displayd_count.toLocaleString('en-US')} ${following_text}`

document.body.insertBefore(accurate, document.body.firstChild)
